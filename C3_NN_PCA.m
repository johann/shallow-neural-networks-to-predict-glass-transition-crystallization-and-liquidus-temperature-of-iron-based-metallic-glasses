% folderName ="/save/NN";
path ='D:\0Experimente';

% init RMSE array
RMSE_=zeros(100,3,2);

% number of iterations for each experiment
NNTraining = 100;

for j=1:2
for i=1:NNTraining
% load data
fname = "processedData_v19_30.mat";
load(fname);

if j == 1

    % apply PCA to dataset inputs
    [coeff,PCA,~,~,~,~] = pca([Xtrain;Xval;Xtest]);
    
    % get mean of dataset inputs
    MEAN = mean([Xtrain;Xval;Xtest]);
    
    % find number of training data
    m1 = size(Xtrain,1);
    % find number of training + validation data
    m2 = size(Xval,1)+m1;
    
    % normalise PCA
    PCA = normalize(PCA,2,'range',[0 1]);
    
    % split PCA into datasets
    Xtrain = PCA(1:m1,:);
    Xval = PCA(m1+1:m2,:);
    Xtest = PCA(m2+1:end,:);

end

rng('shuffle');

% set layersize
Layersize=[20];

% create untrained net
net = fitnet(Layersize, "trainbr");

% set max fail param
maxFail = 100;
% assign divide function for datasets
net.divideFcn='divideblock';
% assign dataset sizes
net.divideParam.trainRatio = 0.89999;
net.divideParam.valRatio = 1-0.89999;
net.divideParam.testRatio = 0;
% assign maxFail parameter for early stopping
net.trainParam.max_fail= maxFail;
% assign data to inputs/outputs
Input=[Xtrain;Xval]';
Output=[Ytrain;Yval]';

% assign number of epochs
net.trainParam.epochs = 100;
% hide training window
net.trainParam.showWindow=0;

% assign activation functon
net.layers{1}.transferFcn = "logsig";
% net.layers{2}.transferFcn = "logsig";

% train neural net
[trainedNet,tr] = train(net,Input,Output);

% get prediciton + error
Ypred = trainedNet([Xtest]');
error = Ypred'-[Ytest];

% get RMSE of error
RMSE = sqrt(mean(error.^2));

% un-normaise RMSE
RMSE_absolut = RMSE.*(maxTemp-minTemp);

RMSE_(i,:,j)=RMSE_absolut;

% assign fileName 
if j==1

fileName = 'NN_v19_early_100_Topologie_30_PCA';
elseif j==2

fileName = 'NN_v19_early_100_Topologie_30_NO_PCA';
end

% assemble filename
fileNameSave = append(fileName,num2str(Layersize),'_',num2str(0),'_',num2str(0),'_',num2str(i));

% [path,~,~]=fileparts(which(mfilename));
% path = append(path,folderName);
% 
% if ~exist(path, 'dir')
%        mkdir(path)
% end

% save
saveParfor(path,fileNameSave,tr,trainedNet,error,RMSE,maxTemp,minTemp,Xtrain,Xval,Xtest,Yval,Ytrain,Ytest);
end
end
function saveParfor(path,fileNameSave,tr,trainedNet,error,RMSE,maxTemp,minTemp,Xtrain,Xval,Xtest,Yval,Ytrain,Ytest)

save(fullfile(path,fileNameSave), "tr","trainedNet","error","RMSE","maxTemp","minTemp",...
    "Xtrain","Xval","Xtest","Yval","Ytrain","Ytest");
end