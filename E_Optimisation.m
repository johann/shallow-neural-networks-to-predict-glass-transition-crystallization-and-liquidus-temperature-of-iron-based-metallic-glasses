%% doc string
% determine boundary conditions for optimiser and use genetic algorithm to
% find a decent local optimum
clear all
%% Inputs

% options get added on top like constraints and free variables
fileNameSave = 'Viktor';

%savepath 
SavePath = '\\cifs\cluster\home\fp633563\WinDocuments\MATLAB\opt\lukas';

%filenames for Data and NN
fDataName= "processedData_v21_30.mat";
fNeuralNetworkName = 'UltraNetz_v21_30.mat';

%free variables for the optimizer
ElementsToOptimize={'Fe','B','Si','Mo','C','Nb'};

%lower and upper bound for free variables(-Inf and Inf will use Data min/max
ElementsToOptimize_LowerBound_Mol = [25 4 -Inf -Inf -Inf -Inf];
ElementsToOptimize_UpperBound_Mol = [Inf Inf Inf Inf Inf Inf ];

%uses percentage(i) as ub, if ub==Inf -- percentage(Data,90) describes ub where 90
%percent of data lies. predefined ub and lb overwrites percentage--
%if ub ==lb variable is removen and added to constant after warning--
%if no percentage is wanted, set 'Percentage=[1];'
Percent=[95 1];

ConstantNonzeroElements={'Cr'};

ConstantNonzeroElements_Mol = [10];

%nonlinear constraints options ({'Inoue','Inoue+convHull','convHull','none','all'})
Constraints={'Inoue','Inoue+convHull','convHull','none'};

%determines if nonlinear constraints are parallelized (or percentage)
Parallel=true;

%seed
rng(55)

%load data
load(fDataName)
load(fNeuralNetworkName)

%Data for convHull and lb/ub
Data=[Xtrain;Xval;Xtest];


%% run
[x,fval,flag,out,solution] = optimizeComp(Parallel,fileNameSave,Percent,ElementsToOptimize_UpperBound_Mol,ElementsToOptimize_LowerBound_Mol,ConstantNonzeroElements,ConstantNonzeroElements_Mol,ElementsToOptimize,Networks,minTemp,maxTemp,ElementsHeaderLeft,maxComp,minComp, AtomicRadii,Data,Constraints,Enthalpy);
%% functions (need to be nested for optimizations and parallelization reasons) parfor needs a seperate save function to be able to save variables and genetic algorithm needs nested nonlcon and fun function to access variables not pased directly

function [OptimizedComp,fval,flag,out,solution,GammaM] = optimizeComp(Parallel,fileNameSave,Percent,ElementsToOptimize_UpperBound_Mol,ElementsToOptimize_LowerBound_Mol,ConstantNonzeroElements,ConstantNonzeroElements_Mol,ElementsToOptimize,Networks,minTemp,maxTemp,ElementsHeaderLeft,maxComp,minComp, AtomicRadii,Data,Constraints,Enthalpy)

if size(ElementsToOptimize_LowerBound_Mol)~= size(ElementsToOptimize_UpperBound_Mol)
    error('lb and ub have different sizes')
end

for i=1:length(ElementsToOptimize_LowerBound_Mol)
    if ElementsToOptimize_LowerBound_Mol(i)>= ElementsToOptimize_UpperBound_Mol(i)
        error(num2str(i) +'-th lb is larger than ub')
    end
end
%% Look up 'mixing enthalpy in Fe' for each element
if any(strcmp(Constraints,'Inoue')) || any(strcmp(Constraints,'all'))|| any(strcmp(Constraints,'Inoue+convHull'))
    ElementEnthalpy = zeros(size(ElementsHeaderLeft));
    % loop over elements in elements header
    for i = 1:length(ElementsHeaderLeft)
        element = ElementsHeaderLeft(i);

        % find index of element in enthalpy cell array
        index = find(ismember(Enthalpy, element));

        % use index to save the enthalpy of element to array
        ElementEnthalpy(i) = str2double(Enthalpy(index, 2));
    end
end
%%
if Parallel && length(Constraints)>1
%
    for constraints_Counter=1:length(Constraints)
           for Percent_counter=1:length(Percent) 
            [OptimizedComp,fval,flag,out,solution,GammaM] = optimizeComp_parallel(fileNameSave,ElementEnthalpy,Percent_counter,Percent,ElementsToOptimize_UpperBound_Mol,ElementsToOptimize_LowerBound_Mol,ConstantNonzeroElements,ConstantNonzeroElements_Mol,ElementsToOptimize,Networks,minTemp,maxTemp,ElementsHeaderLeft,maxComp,minComp, AtomicRadii,Data,constraints_Counter,Constraints,Enthalpy);
           end
    end

elseif Parallel && length(Percent)>1
%
    for Percent_counter=1:length(Percent)
        for constraints_Counter=1:length(Constraints)
            [OptimizedComp,fval,flag,out,solution,GammaM] = optimizeComp_parallel(fileNameSave,ElementEnthalpy,Percent_counter,Percent,ElementsToOptimize_UpperBound_Mol,ElementsToOptimize_LowerBound_Mol,ConstantNonzeroElements,ConstantNonzeroElements_Mol,ElementsToOptimize,Networks,minTemp,maxTemp,ElementsHeaderLeft,maxComp,minComp, AtomicRadii,Data,constraints_Counter,Constraints,Enthalpy);
        end
    end
else
    if ~Parallel
         [OptimizedComp,fval,flag,out,solution,GammaM] =optimizeComp_parallel(fileNameSave,ElementEnthalpy,Percent_counter,Percent,ElementsToOptimize_UpperBound_Mol,ElementsToOptimize_LowerBound_Mol,ConstantNonzeroElements,ConstantNonzeroElements_Mol,ElementsToOptimize,Networks,minTemp,maxTemp,ElementsHeaderLeft,maxComp,minComp, AtomicRadii,Data,constraints_Counter,Constraints,Enthalpy);
    end
end
end
function [OptimizedComp,fval,flag,out,solution,GammaM] = optimizeComp_parallel(fileNameSave,ElementEnthalpy,Percent_counter,Percent,ElementsToOptimize_UpperBound_Mol,ElementsToOptimize_LowerBound_Mol,ConstantNonzeroElements,ConstantNonzeroElements_Mol,ElementsToOptimize,Networks,minTemp,maxTemp,ElementsHeaderLeft,maxComp,minComp, AtomicRadii,Data,constraints_Counter,Constraints,Enthalpy)

%% find Elements and set ids

for i=1:length(ElementsToOptimize)
    ElementsToOptimize_id(i) = find(strcmp(ElementsHeaderLeft,ElementsToOptimize(i)));
end
for i=1:length(ConstantNonzeroElements)
    ConstantNonzeroElements_id(i) = find(strcmp(ElementsHeaderLeft,ConstantNonzeroElements(i)));
end

%% normalize ub and lb and 

%check if in mol or percent
if any(ElementsToOptimize_UpperBound_Mol>1 & ElementsToOptimize_UpperBound_Mol ~= Inf) 
    ElementsToOptimize_LowerBound_Mol=ElementsToOptimize_LowerBound_Mol./100;
end
if any(ElementsToOptimize_UpperBound_Mol>1)
    ElementsToOptimize_LowerBound_Mol=ElementsToOptimize_LowerBound_Mol./100;
end
if any(ConstantNonzeroElements_Mol>1)
    ConstantNonzeroElements_Mol=ConstantNonzeroElements_Mol./100;
end
%normalize
for i=1:length(ElementsToOptimize_UpperBound_Mol)
    if ElementsToOptimize_UpperBound_Mol(i) ~= Inf
        ElementsToOptimize_UpperBound_Mol(i)=(ElementsToOptimize_UpperBound_Mol(i)-minComp(ElementsToOptimize_id(i)))./(maxComp(ElementsToOptimize_id(i))-minComp(ElementsToOptimize_id(i)));
    end
    if ElementsToOptimize_LowerBound_Mol(i) ~= -Inf
        ElementsToOptimize_LowerBound_Mol(i)=(ElementsToOptimize_LowerBound_Mol(i)-minComp(ElementsToOptimize_id(i)))./(maxComp(ElementsToOptimize_id(i))-minComp(ElementsToOptimize_id(i)));
    end
end
for i=1:length(ConstantNonzeroElements_Mol)
    ConstantNonzeroElements_Mol(i)=(ConstantNonzeroElements_Mol(i)-minComp(ConstantNonzeroElements_id(i)))./(maxComp(ConstantNonzeroElements_id(i))-minComp(ConstantNonzeroElements_id(i)));
end


%% set bounds and check if free variables need to be set to constants 
%% ,because the set lower bound is higher than where percantage amount of Data lies in

if length(Percent)==1 && Percent==1 
    %if all data is used set ub and lb as min/max from data
    ub=max(Data(:,ElementsToOptimize_id));
    lb=min(Data(:,ElementsToOptimize_id));
    for i=1:length(ub)
        if ElementsToOptimize_UpperBound_Mol(i) ~=Inf 
            ub(i) = ElementsToOptimize_UpperBound_Mol(i);
        end
        if ElementsToOptimize_LowerBound_Mol(i) ~= -Inf 
            lb(i) = ElementsToOptimize_LowerBound_Mol(i);
        end
    end
else
    %if percent array is filled up check for constant variables

    ub = prctile(Data(:,ElementsToOptimize_id),Percent(Percent_counter));        
    lb=min(Data(:,ElementsToOptimize_id));
    for i=1:length(lb)
        if ElementsToOptimize_LowerBound_Mol(i) ~= -Inf 
            lb(i) = ElementsToOptimize_LowerBound_Mol(i);
        end
    end
     for i=length(ub):-1:1
        if ub(i) <= lb(i)
            if lb(i)==0
                ElementsToOptimize_id(i)=[];
                ElementsToOptimize_UpperBound_Mol(i)=[];
                ElementsToOptimize_LowerBound_Mol(i)=[];
                lb(i)=[];
                ub(i)=[];
            else

                ConstantNonzeroElements{end+1}=ElementsToOptimize{i};
                ConstantNonzeroElements_id(end+1)=ElementsToOptimize_id(i);
                ConstantNonzeroElements_Mol(end+1)=lb(i);
                ElementsToOptimize(i)=[];
                ElementsToOptimize_id(i)=[];
                ElementsToOptimize_UpperBound_Mol(i)=[];
                ElementsToOptimize_LowerBound_Mol(i)=[];
                lb(i)=[];
                ub(i)=[];
            end

        end
     end
end

%% set logical array of free Variables
log = zeros(1,length(ElementsHeaderLeft));
log(ElementsToOptimize_id) = 1;
log=logical(log);

%% get convHull
convHull.Vertices=[];
convHull.Vol=[];
if strcmp(Constraints{constraints_Counter},'Inoue+convHull') || strcmp(Constraints{constraints_Counter},'convHull') || strcmp(Constraints{constraints_Counter},'all')

    if length(ElementsToOptimize)>8
        error('too many dimensions to build convex Hull - lower DImensions or remove convHull');
    else
        [k,Vol] = convHulln(Data(:,ElementsToOptimize_id));
        Vertices=Data(k,ElementsToOptimize_id);
        convHull.Vertices=Vertices;
        convHull.Vol=Vol;
    end
end
%% options
options_ga = optimoptions('ga','Display','iter','UseVectorized',false,"PopulationSize",200,'MaxTime',60,...
    "ConstraintTolerance",1e-2,'MaxStallGenerations',10,'CreationFcn','gacreationlinearfeasible');


%% Linear Equality (Sum of Elements is equal to 1)

maxComp_=maxComp(log);
minComp_=minComp(log);

Aeq = (maxComp_-minComp_);
beq = 1 - sum(minComp_) - (sum(ConstantNonzeroElements_Mol.*(maxComp(ConstantNonzeroElements_id)-minComp(ConstantNonzeroElements_id))+minComp(ConstantNonzeroElements_id)));

%% Linear Inequality
Aineq =[];
bineq =[];

%% apply genetic algorithm
Dimension=sum(log);

[x,fval,flag,out,population,scores] = ga(@fun,Dimension,Aineq,bineq,Aeq,beq,lb,ub,@nonlcon,options_ga);
solution.population=population;
solution.scores=scores;




x_=zeros(1,length(log));
x_(ConstantNonzeroElements_id) = ConstantNonzeroElements_Mol;
x_(ElementsToOptimize_id) = x;

OptimizedComp = x_.*(maxComp-minComp)+minComp;


GammaM=1-sqrt(fval);

FreeElements='';
for i=1:length(ElementsToOptimize)
    FreeElements=append(FreeElements,ElementsToOptimize{i});
end
StaticElements='';
for i=1:length(ConstantNonzeroElements)
    StaticElements=append(StaticElements,ConstantNonzeroElements{i});
end
fileNameSave_ = append(fileNameSave,'_GA_',num2str(length(solution.population)),'_constr_',Constraints{constraints_Counter},'_P_',num2str(Percent(Percent_counter)),'_',FreeElements,'_Const_',StaticElements);

saveParfor(path,fileNameSave_,x,fval,flag,out,solution,OptimizedComp,GammaM,options_ga,convHull);

%% function nonlcon non linear boundary condition

    function [c,ceq] = nonlcon(x)
     
if strcmp(Constraints{constraints_Counter},'Inoue') || strcmp(Constraints{constraints_Counter},'all') || strcmp(Constraints{constraints_Counter},'Inoue+convHull')

x_=zeros(1,length(log));
x_(ConstantNonzeroElements_id) = ConstantNonzeroElements_Mol;
x_(ElementsToOptimize_id) = x;
x_=x_.*(maxComp-minComp)+minComp;

c(1) = 10*(0.5 - mismatchEntropy(x_));
c(2) =  14 + ( 4 * x_ * ElementEnthalpy .* x_(1));
ceq=[];

elseif strcmp(Constraints{constraints_Counter},'all')|| strcmp(Constraints{constraints_Counter},'Inoue+convHull')
c=[];
ceq = inchull(x', Vertices')-1;

else
ceq =[];
c=[];
end
end

%% objective function

function [obj] = fun(x)

x_=zeros(1,length(log));
x_(ConstantNonzeroElements_id) = ConstantNonzeroElements_Mol;
x_(ElementsToOptimize_id) = x;

net =Networks{1,1};
YPred =net(x_');
for i=1:length(Networks)
    net = Networks{i,1};
    YPred =YPred + net(x_');
end
YPred = YPred./length(Networks);

Tgnorm = YPred(1,:);
Txnorm = YPred(2,:);
Tlnorm = YPred(3,:);
Tg = Tgnorm .* (maxTemp(1)-minTemp(1)) + minTemp(1);
Tx = Txnorm .* (maxTemp(2)-minTemp(2)) + minTemp(2);
Tl = Tlnorm .* (maxTemp(3)-minTemp(3)) + minTemp(3);
gammaM = (2 .* Tx - Tg) ./ Tl;
obj = (1-gammaM).^2;


end

    function S = mismatchEntropy(x)

S = entropyMix(x, AtomicRadii, ElementsHeaderLeft);

end

end
function saveParfor(path,fileNameSave,x,fval,flag,out,solution,OptimizedComp,GammaM,options_ga,convHull)
cd(path);
save(fileNameSave, "x","fval","flag","out","solution","OptimizedComp","GammaM",'options_ga','convHull');

end
