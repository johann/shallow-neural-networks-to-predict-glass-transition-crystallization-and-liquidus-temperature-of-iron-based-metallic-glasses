%% Add or generate paths
foName ="/save/Results";
[fpath,~,~]=fileparts(which(mfilename));
fpath = append(fpath,foName);
% create folder if not yet exists
 if ~exist(fpath, 'dir')
       mkdir(fpath)
 end

addpath(pwd + "\save\Experiments");

%% Initialize
% Element threshold array
eThresh=[30];

% init layer size arrays
Layer1 = [18 19 20 21 22];
Layer2 = [0 1 2 3];
Layer3 = [0];

% set number of iterations for each permutation
NNTraining = 100;

% preallocation arrays for results
allRMSE= cell(length(eThresh));
RMSE_Mean = cell(length(eThresh));
RMSE_Mean_allT = cell(length(eThresh));
RMSE_Std = cell(length(eThresh));
RMSE_Std_allT = cell(length(eThresh));
RMSE_Median = cell(length(eThresh));
RMSE_Median_allT = cell(length(eThresh));

YPred=zeros([size(Ytest),NNTraining]);

% for loop for element threshold 
for counter=1:numel(eThresh)

fileName = 'NN_noVik_v22_100_Topology_';
fileName = append(fileName,num2str(eThresh(counter)),'_');
% folderName ="/save/NN";

% for loops for layer sizes
for layer1Counter = 1:length(Layer1)
    for layer2Counter =1:length(Layer2)
     for layer3Counter = 1:length(Layer3)
        temp = [];
        for i=1:NNTraining
             % get layer sizes of current iteration
             layer1 = Layer1(layer1Counter);
             layer2 = Layer2(layer2Counter);
             layer3 = Layer3(layer3Counter);
             % assemble filename
             fileName_temp = append(fileName,num2str(layer1),'_',num2str(layer2),'_',num2str(layer3),'_',num2str(i));
             % load file
             load(fileName_temp);
             % read results
             temp = [temp;RMSE];
        end
        % Saving RMSE Data in large nested cell
        allRMSE{counter}{layer1Counter,layer2Counter,layer3Counter} = temp;
     end
    end
end
%% Save mean, medians etc. for comparisons
RMSE_Mean{counter} = cellfun(@mean, allRMSE{counter}, 'UniformOutput', false);
RMSE_Mean_allT{counter} = cell2mat(cellfun(@mean, RMSE_Mean{counter}, 'UniformOutput', false)); % get average RMSE on all three output temperatures
RMSE_Std{counter} = cellfun(@std, allRMSE{counter}, 'UniformOutput', false);
RMSE_Std_allT{counter} = cell2mat(cellfun(@mean, RMSE_Std{counter}, 'UniformOutput', false));
RMSE_Median{counter} = cellfun(@median, allRMSE{counter}, 'UniformOutput', false);
RMSE_Median_allT{counter} = cell2mat(cellfun(@median, RMSE_Median{counter}, 'UniformOutput', false));
end


%% Plot Topology results

f1 = figure();
% create heatmap
imagesc(Layer1, Layer2, RMSE_Median_allT{counter}')
%setup axis correctly
set(gca,'YDir','normal')
curtick = get(gca, 'XTick');
set(gca, 'XTick', unique(round(curtick)))
curtick = get(gca, 'YTick');
set(gca, 'YTick', unique(round(curtick)))
% set font
set(gca,'Fontsize', 18,'FontName','MyriadPro','LineWidth',1);

colormap = flipud(copper);
c = colorbar;
c.Title.String="RMSE [K]";
c.FontSize=18;
c.FontName='Myriad Pro';

% add axis labels
xlabel('Neurons in layer 1 [-]')
ylabel('Neurons in layer 2 [-]')

% add title
title("\Lambda = 30 Median RMSE",'FontWeight','normal','FontName','Myriad Pro','FontSize',18)

saveas(f1, append(fpath, "\topology_v3.png"))
saveas(f1, append(fpath, "\topology_v3.fig"))
%% Optimal Topology
[row, col] = find(RMSE_Median_allT{counter} == min(RMSE_Median_allT{counter}, [], "all")); %assuming RMSE_Median is the best metric
disp("Optimal topology found at " + num2str(Layer2(col)) + " neurons in the first and " + num2str(Layer1(row)) + " neurons in the second Layer.")

%% Save mean, medians etc. for comparisons
fiName = "resultdata_v22";
save(fullfile(fpath,fiName), "Xtrain","Xval","Xtest","Yval","Ytrain","Ytest","allRMSE","RMSE_Mean_allT", "RMSE_Median_allT", "RMSE_Std_allT");
