Code was written using MATLAB 2022a. This work is based on the publications:

"Shallow neural networks to predict glass transition, crystallization and liquidus temperature of iron-based metallic glasses"
-doi.org/10.1088/1757-899X/1147/1/012012
-Setup for one neural network per regressand (Tg, Tx, Tl)
-First Version Branch

"Data driven development of iron-based metallic glasses using artificial neural networks"
-doi.org/10.1016/j.jallcom.2023.172895
-Vast improvement of database
-Single ANN for all regressands
-Second Version Branch

Currently working on Third Version; best performance so far.

The latest database is publicly available at doi.org/10.18154/RWTH-2023-09114 