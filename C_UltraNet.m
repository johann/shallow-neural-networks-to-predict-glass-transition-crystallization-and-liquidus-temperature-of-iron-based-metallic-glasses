% save path
path = [pwd + "\save"];

% set layer size
Layersize = [21, 2];

% number of nets
NNTraining = 100;

% name of neural network save file
fname = append("UltraNetz_v3.mat");

% load data
%load("processedData_noVik_v22.mat")

% init arrays for weights etc
RMSEarray=[];
Networks =cell(1,1);

% iterate over nets to be trained
for ii=1:NNTraining

rng('shuffle');

% create untrained net
net = fitnet(Layersize, "trainbr");

% set max fail param
maxFail = 1000;

% assign data divide function
net.divideFcn='divideblock';

% assign dataset sizes
net.divideParam.trainRatio = 0.89999;
net.divideParam.valRatio = 1-0.89999;
net.divideParam.testRatio = 0;

% assign maxFail param for early stopping
net.trainParam.max_fail= maxFail;

% assign datasets to input/outputs
Input=[Xtrain;Xval]';
Output=[Ytrain;Yval]';

% assign # of epochs
net.trainParam.epochs = 1000;
% hide training window
net.trainParam.showWindow=0;

% assign activation function to layer 1
net.layers{1}.transferFcn = "logsig";

% train neural net
[trainedNet,tr] = train(net,Input,Output);

% get prediciton and error
Ypred = trainedNet(Xtest');
error = Ypred'-Ytest;

% calculate RMSE for error
RMSE = sqrt(mean(error.^2));

% assemble save name
fileNameSave = append(fileName,'_',num2str(ii));

    % Do not save NN with high RMSE
    if any(RMSE >[30 30 30])
    
        NNTraining=NNTraining-1;
        saveParfor(path,fileNameSave,tr,trainedNet,error,RMSE,Xtrain,Xval,Xtest,Yval,Ytrain,Ytest);
    
    else
        RMSEarray=[RMSEarray;RMSE];
        NNTraining=NNTraining+1;
        Networks{NNTraining,1} = trainedNet;
        saveParfor(path,fileNameSave,tr,trainedNet,error,RMSE,Xtrain,Xval,Xtest,Yval,Ytrain,Ytest);
    end
end

% save
save(fname,'Networks')

% init predictions array
YPRED = zeros(size(Ytest));

% iterate over networks and fill predictions array
for ii=1:NNTraining
    net = Networks{ii,1};
YPRED = YPRED + net(Xtest')';
end

% get mean prediction
YPRED=YPRED./NNTraining;

% find error between mean prediciton and data
error = YPRED-Ytest;

% calc RMSE of mean
RMSE_MeanNet = sqrt(mean(error.^2,1));

% get mean of RMSE
RMSE_Mean = mean(RMSEarray);

% save
function saveParfor(path,fileNameSave,tr,trainedNet,error,RMSE,Xtrain,Xval,Xtest,Yval,Ytrain,Ytest)

save(fullfile(path,fileNameSave), "tr","trainedNet","error","RMSE",...
    "Xtrain","Xval","Xtest","Yval","Ytrain","Ytest");
end