clear all
close all

%%
% % Determine where your m-file's folder is.
% %find the filepath
mainDir = fileparts(which(mfilename));

%add path
addpath(genpath(pwd));

% Folder for saving
foName ="\save";

% load input .mat
[fiName,fpath] = uigetfile(mainDir + foName + '\' + '*.mat');
load(fiName);

% filename for saving, overwrite old fiName
fiName= "processedData_noVik_v22";

%% set parameters

% init random function
rng('shuffle')

% temperature correction parameters
BTg = 6.29;
BTx = 13.35;

%allowed max difference between standard deviations in the output ( in
%percentage)
STD_Diff = 0.05;

% Dataset sizes
pTrain = 0.8;
pVal = 0.1;
pTest = 1-pTrain-pVal;

% element frequency threshold
eThresh = 30;

% calculate mixing enthalpy and mean atomic radius

% new column in table for mixing enthalpy 
[len, ~] = size(Data_tbl);
Data_tbl.mixingEnthalpy = zeros(len, 1);
Data_tbl.mAtomicRadii = zeros(len, 1);

%% mixing enthalpies

% loop over all elements listed in table and sum mixing enthalpies
% separately + determine mean atomic radius
for ii = 1:length(ElementsHeader)

    % get current element string
    currentElement = ElementsHeader(ii);

    % find row index of element in enthalpy cell
    idxEnthalpy = find(strcmp(Enthalpy.(1), currentElement));
    %[~, elementNumberRadii] = ismember(currentElement, AtomicRadii(:, 1));
    % if element is not found in enthalpy cell, discard it and continue 
    % with next iteration
    if idxEnthalpy ~= 0

        % get vector with mass fraction of the element for all alloys
        elementFractionComp = Data_tbl.(currentElement);
    
        % Use index to extract mixing enthalpy for the current element
        elementEnthalpy = str2double(Enthalpy{idxEnthalpy, 2});

        % get vector of iron content
        ironContent = Data_tbl.Fe;
    
        % get vector of partial mixing enthalpies
        elementmixingEnthalpy = 4 * elementFractionComp .* elementEnthalpy .* ironContent; % how did we get 4 in here?
    
        % sum the partial mixing enthalpies
        Data_tbl.mixingEnthalpy = Data_tbl.mixingEnthalpy + elementmixingEnthalpy;
    else
        warning(['The mixing enthalpy for Element '+ "'" + currentElement + "'" + ' was not found.']);
    end


    % find row index of element in atomicradii cell
    idxAtomicRadii = find(strcmp(AtomicRadii.(1), currentElement));

    % if element is not found in atomicradii cell, discard it and continue 
    % with next iteration
    if idxAtomicRadii ~= 0
        % get vector with mass fraction of the element for all alloys
        elementFractionComp = Data_tbl.(currentElement);
    
        % Use index to atomic radius of the current element
        atomicRadius = str2double(AtomicRadii{idxAtomicRadii, 2});
    
        % get vector of partial atomic radii
        elementPartialRadius = elementFractionComp * atomicRadius;
        Data_tbl.("mAtomicRadii") = Data_tbl.("mAtomicRadii") + elementPartialRadius;
    else
        warning(['The Atomic Radius for Element '+ "'" + currentElement + "'" + ' was not found.']);
    end
end

%% sigma atomic radii

% initialise variance
variance = zeros(len, 1);

% loop over all elements listed in table and determine variance of atomic 
% radii separately
for ii = 1:length(ElementsHeader)

    % get current element string
    currentElement = ElementsHeader(ii);

    % find row index of element in atomicradii cell
    idxAtomicRadii = find(strcmp(AtomicRadii.(1), currentElement));
    
    % if element is not found in atomicradii table, discard it and continue 
    % with next iteration
    if idxAtomicRadii == 0
        continue
    end

    % get vector with atomic fraction of the element for all alloys
    elementFractionComp = Data_tbl.(currentElement);

    % Use index to atomic radius of the current element
    atomicRadius = str2double(AtomicRadii{idxAtomicRadii, 2});
  
    pvariance = elementFractionComp .* (Data_tbl.mAtomicRadii - atomicRadius) .^ 2;
    variance = variance + pvariance;

end

% use variance to calculate sd and add to table
Data_tbl.("sdAtomicRadii") = sqrt(variance);

%% correct Tg & Tx

medianHeatingRate = median(Data_tbl.HeatingRate, 'omitnan');

% replace NaN values in the heating rate with the default e.g. Median Heating Rate of 20 K/min
Data_tbl.HeatingRate(isnan(Data_tbl.HeatingRate)) = medianHeatingRate;

% use the calcLN function to correct the temperatures for heatingrate
% save the result to a new column in the table
[Data_tbl.Tg_c, ~] = calcLN(Data_tbl.Tg , Data_tbl.HeatingRate, BTg);
[Data_tbl.Tx_c, ~] = calcLN(Data_tbl.Tx , Data_tbl.HeatingRate, BTx);
Data_tbl.Tl_c = Data_tbl.Tl;

Data_tbl.dT = Data_tbl.Tx - Data_tbl.Tg;
Data_tbl.dT_c = Data_tbl.Tx_c - Data_tbl.Tg_c;

%% atomic radii classification mass fractions

% create columns in the table for the mass fractions
Data_tbl.smallRadii = zeros(len, 1);
Data_tbl.intermediateRadii = zeros(len, 1);
Data_tbl.bigRadii = zeros(len, 1);

% iterate over all elements 
for ii = 1:length(ElementsHeader)

    % get element name
    currentElement = ElementsHeader(ii);

    % find element name in atomicradiiclassification cell array
    idxRadClass = find(strcmp(AtomicRadiiClassification{:, 1}, currentElement));

    % check for classification of the current element by checking for "1"
    % in the corresponding columns and add the mass fraction to the
    % relevant column in the table
    if ~isempty(idxRadClass)

        small = AtomicRadiiClassification{idxRadClass, 3};
        intermediate = AtomicRadiiClassification{idxRadClass, 4};
        big = AtomicRadiiClassification{idxRadClass, 5};
    
    %refer to excel sheet check whether element is small, big ,
    %intermediate

        if small == 1
            Data_tbl.smallRadii = Data_tbl.smallRadii + Data_tbl.(currentElement);
        elseif intermediate == 1
            Data_tbl.intermediateRadii = Data_tbl.intermediateRadii + Data_tbl.(currentElement);
        elseif big == 1
            Data_tbl.bigRadii = Data_tbl.bigRadii + Data_tbl.(currentElement);
        else
            continue
        end
    else
    warning(['The Atomic Radius Class for Element '+ "'" + currentElement + "'" + ' was not found.']);
    end
end

ProcessedDataUnfiltered = Data_tbl;

%% delete data beneath threshhold

% drop rows with rare elements that are only present in <threshold
% samples

% initialise arrays for columns and rows that will be dropped
deleteColumns = [];
deleteRows = [];

% copy table to an array
DataArray = table2array(Data_tbl);

% loop over each element
for ii = 2:length(ElementsHeader) + 1

    % if the number of non zero values in the column doesn't exceed the
    % threshold add column to bad column array
    if sum(DataArray(:, ii)~=0) < eThresh
        deleteColumns = [deleteColumns, ii];

        % find the non zero elements of the bad column and add the
        % respective row indeces to the bad rows list
        deleteRows = [deleteRows, find(DataArray(:, ii))'];
    end
end

% save deleted samples before deleting the rows and columns
Deleted_Data = Data_tbl(deleteRows, :);

% delete rows and columns (order doesn't matter)
Data_tbl(:, deleteColumns) = [];
Data_tbl(deleteRows, :) = [];

DataArray(:, deleteColumns) = [];
DataArray(deleteRows, :) = [];

% find number of remaining elements in the table
NumberOfElementsLeft = length(ElementsHeader) - length(deleteColumns);

NumberOfOutputs = 3;

% initialise ElementsHeaderLeft
ElementsHeaderLeft = ElementsHeader;

% Remove the deleted elements from the ElementsHeader left
deleteColumns = deleteColumns - 1;
ElementsHeaderLeft(deleteColumns, :) = [];

% delete same samples
CONSTANT_NUMBER = -5000000000; %NaNs are replaced with constant number

DataArray(isnan(DataArray)) = CONSTANT_NUMBER; %unique doesnt work with nan
[~,idx,~] = unique(DataArray(:,2:NumberOfElementsLeft+NumberOfOutputs+1),'rows');

Data_tbl=Data_tbl(idx,:); % deleting the columns of elements that are underrepresented 

%% remove known outliers from data

% find temperature and elements range
idxFe = find(strcmp(Data_tbl.Properties.VariableNames , 'Fe'),1);
idxTgc = find(strcmp(Data_tbl.Properties.VariableNames , 'Tg_c'),1);
idxTgl = find(strcmp(Data_tbl.Properties.VariableNames , 'Tl_c'),1);

ElementCols = [idxFe idxFe+NumberOfElementsLeft-1]; % get columns of Elements
TemperatureCols = [idxTgc idxTgl]; % get columens of temperatures

% There are two stray alloys that contain excessive amounts of Mo
if any(strcmp(ElementsHeaderLeft,'Mo'))
    Data_tbl(Data_tbl.Mo > 0.2, :) = [];
end

% There is one alloy containing excessive Ga
if any(strcmp(ElementsHeaderLeft,'Ga'))
    Data_tbl(Data_tbl.Ga > 0.1, :) = [];
end

% There are three alloys containing excessive Si
if any(strcmp(ElementsHeaderLeft,'Si'))
    Data_tbl(Data_tbl.Si > 0.18, :) = [];
end
% remove any alloys with too little iron content
Data_tbl(Data_tbl.Fe < 0.18, :) = [];


%% average same compositions
[Data_tbl_unique] = combineDuplicates(Data_tbl,ElementCols,TemperatureCols);

% normalization is done internally within matlab functions
% grab Input/Output data for the neural network from table 
Input = Data_tbl_unique{:,ElementCols(1):ElementCols(2)};
Output = Data_tbl_unique{:,TemperatureCols(1):TemperatureCols(2)}; 

% Split data into train, validation and test set
% get balance in all sets
% caution this step is computationally expensive
[Xtrain,Xtest,Xval,Ytrain,Ytest,Yval] = balanceDatasets(Input,Output,pTrain,pVal,STD_Diff); 

%% save processed data
[~, fiName, ~] = fileparts(fiName);
fiName = append(fiName, '_', num2str(eThresh),'.mat');

 % save relevant variables to file
save(fullfile(fpath,fiName), "Data_tbl","Data_tbl_unique", ...
    "Xtrain","Xtest","Xval","Ytrain","Ytest","Yval","ElementCols",...
    "ElementsHeaderLeft","AtomicRadii",...
    "Enthalpy", "ProcessedDataUnfiltered");
