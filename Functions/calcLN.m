function [T20, T10] = calcLN(Tx, hr, b)
% ln approximation 
% hr must be in K/min ?!
if size(Tx) ~= size(hr)
    if size(Tx') == size(hr)
        Tx=Tx';
    else
        error('wrong dimensions');
    end
end
T20 = Tx + b.*log(20./hr);
T10 = Tx + b.*log(10./hr);
end

