function [Table] = delEmpties(Table)
% Delete fully empty rows and columns in table
Table=Table(:,~all(ismissing(Table),1)); % columns
Table=Table(~all(ismissing(Table),2),:); % rows
end