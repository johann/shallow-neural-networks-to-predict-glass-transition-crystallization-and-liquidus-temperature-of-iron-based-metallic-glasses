function [Numarray] = hunToOne(Numarray)
% normalize array entries to total 0-1 instead of 0-100
    if sum(Numarray) >= 99 && sum(Numarray) <= 101
        Numarray = Numarray./100;
    
    elseif sum(Numarray) >= 0.99 && sum(Numarray) <= 1.1
        % abs(sum(Numarray) - 1) < 1e-4 % because 1 ain't 1 in programming
        return
    else
     disp('Error: Contents do not add up to 100 %.')
     disp(Numarray)
    end
end

