function [out, outNum] = calcComp(str)
%Calculate true composition for bracket items

regex1 = '\{.+\}\d*\.?\d+'; % match what's inside {}
regex2 = '\[.+\]\d*\.?\d+'; % match what's inside []
regex3 = '\(.+\)\d*\.?\d+'; % match what's inside ()

if ~isempty(regexp(str, regex1))
    
    %% C
    
    matchStrC = regexp(str, '\}.*', 'match');
    % matchStrC{1} contains e.g. }96Cr4 wherein 96 should be converted to 0.96
    % and multiplied to whats between ] and }
    
    lodC = regexp(matchStrC{1}, '\d+\.?\d*', 'match');
    
    % lod is an array containing the numbers after the } bracket e.g. for 
    % {[(Fe0.6Co0.4)0.75B0.2Si0.05]0.96Nb0.04}96Cr4 
    % lod = [96 4]
    lodC = cellfun(@str2double, lodC);
    lodC = hunToOne(lodC);
    
    % create the new fraction of the outer shell
    % newStrC will then be concentated with the other rest of the (main)
    % string
    newStrC = regexp(matchStrC{1}, '([A-Z][a-z]*\d+\.?\d*)+', 'match');
    newStrC = strNumRep(newStrC, lodC(2:end));
    %% B
    
    matchStrB = regexp(str, '\].*\}', 'match');
    lodB = regexp(matchStrB{1}, '\d+\.?\d*', 'match');
    lodB = cellfun(@str2double, lodB);
    lodB = hunToOne(lodB);
    
    % the new coefficients are calculated
    nlodB = lodB.*lodC(1);
    
    newStrB = regexp(matchStrB{1}, '([A-Z][a-z]*\d+\.?\d*)+', 'match');
    newStrB = strNumRep(newStrB, nlodB(2:end));
    
    %% A
    
    % repeat what has been done for B, the ]-} brackets from ) to ]
    matchStrA = regexp(str, '\).*\]', 'match');
    lodA = regexp(matchStrA{1}, '(\d+)(?:\.(\d{1,2}))?', 'match');
    lodA = cellfun(@str2double, lodA);
    lodA = hunToOne(lodA);
    
    % the new coefficients are calculated
    nlodA = lodA.*nlodB(1);
    
    newStrA = regexp(matchStrA{1}, '([A-Z][a-z]*\d+\.?\d*)+', 'match');
    newStrA = strNumRep(newStrA, nlodA(2:end));
    
    %% Inner brackets (
    
    matchStrIn = regexp(str, '\(.*\)', 'match');
    lodIn = regexp(matchStrIn{1}, '\d+\.?\d*', 'match');
    lodIn = cellfun(@str2double, lodIn);
    lodIn = hunToOne(lodIn);
    
    % the new coefficients are calculated
 
    nlodIn = lodIn.*nlodA(1);
    
    newStrIn = regexp(matchStrIn{1}, '([A-Z][a-z]*\d+\.?\d*)+', 'match');
    newStrIn = strNumRep(newStrIn, nlodIn);
    
    % String concentation
    totalStr = [newStrIn{1} newStrA{1} newStrB{1} newStrC{1}];
    

elseif ~isempty(regexp(str, regex2)) % edgy brackets
    
    %% B is equivalent to C
    
    matchStrB = regexp(str, '\].*', 'match');
    % matchStrC{1} contains e.g. }96Cr4 wherein 96 should be converted to 0.96
    % and multiplied to whats between ] and }
    
    lodB = regexp(matchStrB{1}, '\d+\.?\d*', 'match');
    
    % lod is an array containing the numbers after the } bracket e.g. for 
    % {[(Fe0.6Co0.4)0.75B0.2Si0.05]0.96Nb0.04}96Cr4 
    % lod = [96 4]
    lodB = cellfun(@str2double, lodB);
    lodB = hunToOne(lodB);
    
    % create the new fraction of the outer shell
    % newStrC will then be concentated with the other rest of the (main)
    % string
    newStrB = regexp(matchStrB{1}, '([A-Z][a-z]*\d+\.?\d*)+', 'match');
    newStrB = strNumRep(newStrB, lodB(2:end));
    
    %% A
    
    % repeat what has been done for B, the ]-} brackets from ) to ]
    matchStrA = regexp(str, '\).*\]', 'match');
    lodA = regexp(matchStrA{1}, '\d+\.?\d*', 'match');
    lodA = cellfun(@str2double, lodA);
    lodA = hunToOne(lodA);
    
    % the new coefficients are calculated
    nlodA = lodA.*lodB(1);
    
    newStrA = regexp(matchStrA{1}, '([A-Z][a-z]*\d+\.?\d*)+', 'match');
    newStrA = strNumRep(newStrA, nlodA(2:end));

        %% Inner brackets (
    
    matchStrIn = regexp(str, '\(.*\)', 'match');
    lodIn = regexp(matchStrIn{1}, '\d+\.?\d*', 'match');
    lodIn = cellfun(@str2double, lodIn);
    lodIn = hunToOne(lodIn);
    
    % the new coefficients are calculated
 
    nlodIn = lodIn.*nlodA(1);
    
    % String concentation
    newStrIn = regexp(matchStrIn{1}, '([A-Z][a-z]*\d+\.?\d*)+', 'match');
    newStrIn = strNumRep(newStrIn, nlodIn);
    
    % String concentation
    totalStr = [newStrIn{1} newStrA{1} newStrB{1}];

elseif ~isempty(regexp(str, regex3)) && contains(str, ']')
     %% A
    
    % repeat what has been done for B, the ]-} brackets from ) to ]
    matchStrA = regexp(str, '\).*', 'match');
    lodA = regexp(matchStrA{1}, '\d+\.?\d*', 'match');
    lodA = cellfun(@str2double, lodA);
    lodA = hunToOne(lodA);
        
    newStrA = regexp(matchStrA{1}, '([A-Z][a-z]*\d+\.?\d*)+', 'match');
    newStrA = strNumRep(newStrA, lodA(2:end));

    %% Inner brackets (
    
    matchStrIn = regexp(str, '\(.*\)', 'match');
    lodIn = regexp(matchStrIn{1}, '\d+\.?\d*', 'match');
    lodIn = cellfun(@str2double, lodIn);
    lodIn = hunToOne(lodIn);
    
    % the new coefficients are calculated
 
    nlodIn = lodIn.*lodA(1);
    
    % String concentation
    newStrIn = regexp(matchStrIn{1}, '([A-Z][a-z]*\d+\.?\d*)+', 'match');
    newStrIn = strNumRep(newStrIn, nlodIn);
    
    % String concentation
    totalStr = [newStrIn{1} newStrA{1}];
    
else 
    %%
%     lodIn = regexp(char(str), '\d+\.?\d*', 'match');
%     lodIn = cellfun(@str2double, lodIn);
%     lodIn = hunToOne(lodIn);
%     
%     newStrIn = regexp(char(str), '([A-Z][a-z]*\d+\.?\d*)+', 'match');
%     newStrIn = strNumRep(newStrIn, lodIn);
    
%    totalStr = newStrIn{1};
    totalStr = char(str);
    
    % new else if for case A12B2(C2D4)5E10
    
end
% normalize all the remaining numbers in the ()
% match the final inner brackets
if contains(totalStr, '(')
    inCont = regexp(totalStr, '\(.+\)', 'match');
    inNumOld = regexp(inCont, '\d+\.?\d*', 'match');

    inNumNew = hunToOne(str2double(inNumOld{1,1}));
    %inNumNew = strsplit(num2str(inNumNew));
    newCont = strNumRep(inCont, inNumNew);

    %newCont = replace(inCont{1,1}, inNumOld{1,1}, inNumNew); % replacing everything within () with decimals;
    newCont{1}(1) = [];
    newCont{1}(end) = [];
    totalStr = regexprep(totalStr, inCont, newCont);
end
%always normalize what's inside


%code written by Stephen Cobeldick (Matlab forum)
%compNumbers = str2double(regexp(testcomp,'[\d.]+','match'));
baz = @(s,b)regexprep(s,'(\d+\.?\d*)','${num2str(str2double($1)*str2double(b))}'); %%%
tmp = regexprep(totalStr,'\((([A-Z][a-z]*\d+\.?\d*)+)\)(\d+\.?\d*)','${baz($1,$2)}');    %%%
out = regexp(tmp,'([A-Z][a-z]*)(\d+\.?\d*)','tokens');
out = vertcat(out{:});
% outNum = str2double(out(:,2)); % optional
outNum = out;
end

