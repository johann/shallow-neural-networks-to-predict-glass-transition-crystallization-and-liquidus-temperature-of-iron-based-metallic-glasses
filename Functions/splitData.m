function [Xtrain,Xtest,Xval,Ytrain,Ytest,Yval] = splitData(Input,Output,Ptrain,Pval)
Xtrain=Input(1:floor(length(Input)*Ptrain),:);
Ytrain=Output(1:floor(length(Input)*Ptrain),:);

Xval=Input(ceil(length(Input)*Ptrain):floor(length(Input)*(Ptrain+Pval)),:);
Yval=Output(ceil(length(Output)*Ptrain):floor(length(Output)*(Ptrain+Pval)),:);
if Ptrain+Pval-1<1e-4
    Xtest=Input(ceil(length(Input)*(Ptrain+Pval)):end,:);
    Ytest=Output(ceil(length(Input)*(Ptrain+Pval)):end,:);
else
    Xtest=[];
    Ytest=[];
end
end