function [data_tbl_unique] = combineDuplicates(data_tbl,ElementsRange,TemperatureRange)
% find duplicates and combine them/average their output values

%% sort alloys into two tables with unique alloys and repeated alloys

% first get array with only composition data
compositionArray = table2array(data_tbl(:, ElementsRange(1):ElementsRange(2)));

% find indeces of unique rows
[~, ia, ~] = unique(compositionArray(:, 1:end), "rows");

% add unique rows to no repeats table
data_tbl_unique = data_tbl(ia, :);

% copy full table and delete unique values so only repeats are left
data_tbl_onlyRepeats = data_tbl;
data_tbl_onlyRepeats(ia, :) = [];


%% combine the repeated alloys into the unique alloys



% loop over the unique alloys
for i = 1:height(data_tbl_unique)

    % get composition of current unique alloy
    currentComp = data_tbl_unique(i, ElementsRange(1):ElementsRange(2));

    % check for repeats of the current alloy and determine indeces
    repeatsLogic = ismember(data_tbl_onlyRepeats(:, ElementsRange(1):ElementsRange(2)), currentComp, "rows");

    % if there are repeats in the repeats table combine
    if any(repeatsLogic)

        % get indeces of repeats
        indicesArray = find(repeatsLogic);

        % determine mean of repeats and unique value for Tg_c, Tx_c and
        % Tl_c
        for ii=TemperatureRange(1):TemperatureRange(2)
            data_tbl_unique(i, ii) = num2cell(mean([table2array(data_tbl_unique(i, ii))', table2array(data_tbl_onlyRepeats(indicesArray, ii))'], "omitnan"));
        end
    end   
end
end