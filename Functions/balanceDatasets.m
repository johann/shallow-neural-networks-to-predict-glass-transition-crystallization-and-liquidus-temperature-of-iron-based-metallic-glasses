function [Xtrain,Xtest,Xval,Ytrain,Ytest,Yval] = balanceDatasets(Input,Output,Ptrain,Pval,TolSTD)

Time = 60*30;

[m,n] = size(Output);
%% shuffle
idx = randperm(m);

Input=Input(idx,:);
Output=Output(idx,:);

Ptest =1-Ptrain-Pval;

notNanIdx = find(sum(isnan(Output),2)==0);

%%find extrem values and add to notNaN, so they dont end up in the test/val
%%set
smallestNonZero(1)=min(Input(notNanIdx,1),[],1);
for i=2:size(Input(notNanIdx,:),2)
    Input_temp = unique(nonzeros(Input(notNanIdx,i))); %does sorting and remove duplicates
    if length(Input_temp)>1
        smallestNonZero(i) = Input_temp(2);
    elseif length(Input_temp)==1
        smallestNonZero(i) = Input_temp;
    elseif length(Input_temp)==[]
        smallestNonZero(i) = NaN;
    end
end

minInputIdx = find(any(Input(notNanIdx,:) == smallestNonZero,2));
maxinputIdx = find(any(Input(notNanIdx,:) == max(Input),2));

minOutputIdx = find(any(Output(notNanIdx,:) == min(Output(notNanIdx,:)),2));
maxOutputIdx = find(any(Output(notNanIdx,:) == max(Output(notNanIdx,:)),2));

extremValuesIdx = unique([minInputIdx;maxinputIdx;minOutputIdx; maxOutputIdx]);

notNanIdx(extremValuesIdx)=[];

InputNaN = Input;
InputNaN(notNanIdx,:)=[];
OutputNaN = Output;
OutputNaN(notNanIdx,:)=[];

InputNotNaN = Input(notNanIdx,:);
OutputNotNaN = Output(notNanIdx,:);

mNotNaN = length(OutputNotNaN);

correction = m/mNotNaN;

PtestCorrected = 1-correction*Pval-correction*Ptest;
PvalCorrected = correction * Pval;

[Xtrain,Xtest,Xval,Ytrain,Ytest,Yval] = splitData(InputNotNaN,OutputNotNaN,PtestCorrected,PvalCorrected);
Xtrain = [Xtrain;InputNaN];
Ytrain = [Ytrain;OutputNaN];

stdOutput = std(Output,1,1,'omitnan');

STD = ones(3,n);

tstart = cputime;
%while not all STDs are in tolarance and time is less Time repeat
while(max(STD,[],'all')>TolSTD && (cputime-tstart)<Time)

idx = randperm(mNotNaN);
InputNotNaN=InputNotNaN(idx,:);
OutputNotNaN=OutputNotNaN(idx,:);

[Xtrain,Xtest,Xval,Ytrain,Ytest,Yval] = splitData(InputNotNaN,OutputNotNaN,PtestCorrected,PvalCorrected);
Xtrain = [Xtrain;InputNaN];
Ytrain = [Ytrain;OutputNaN];

%calculate std deviation in percentage [0,1]
STD(1,:) = abs((stdOutput-std(Ytrain,1,1,'omitnan'))./stdOutput);
if size(Ytest,1)>1
    STD(2,:) = abs((stdOutput-std(Ytest,1,1,'omitnan'))./stdOutput);
else
    STD(2,:) =[0 0 0];
end
STD(3,:) = abs((stdOutput-std(Yval,1,1,'omitnan'))./stdOutput);


end

if (cputime-tstart)>Time
    error('maximum time limit reached');
end
end


