function [substr] = strNumRep(substr,numarray)
% replace all the number matches in str with the corresponding entries in
% numarray
spl = regexp(substr{1},'\d+\.?\d*','split');
spl(2,1:end-1) = arrayfun(@num2str,numarray,'uni',0);
substr{1} = sprintf('%s',spl{1:end-1});
end

