function [duplicates] = getDupes(table, ElementsRange)
%% get table with repeat values (but no repeats of repeats)

% first get array with only composition data
compositionArray = table2array(table(:, ElementsRange(1):ElementsRange(2)));

% find indeces of unique rows
[~, ia, ~] = unique(compositionArray(:, 1:end), "rows");

% create repeats table
repeats = table;

% delete unique rows
repeats(ia, :) = [];

% again get array with composition data
repeatsCompositionArray = table2array(repeats(:, ElementsRange(1):ElementsRange(2)));

% find indeces of unique rows in repeats
[~, ia, ~] = unique(repeatsCompositionArray(:, 1:end), "rows");

% only add unique rows of repeats to duplicates table
duplicates = repeats(ia, :);
end