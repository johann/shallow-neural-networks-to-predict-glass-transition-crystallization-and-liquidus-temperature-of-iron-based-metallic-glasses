% add folders
foName ="/save/NN";
[fpath,~,~]=fileparts(which(mfilename));
fpath = append(fpath,foName);
% create folder if not yet exists
 if ~exist(fpath, 'dir')
       mkdir(fpath)
 end

% element threshold parameters
% element threshold array
eThresh=[30];

for counter=1:(numel(eThresh))

% create filename for current iteration
currOutfiName = 'NN_noVik_v22_100_Topology_';
currOutfiName = append(currOutfiName,num2str(eThresh(counter)),'_');

% init layer size arrays
Layer1 = [18 19 20 21 22];
Layer2 = [0 1 2 3];
Layer3 = [0];

% number of neural nets to train
NNTraining = 100;

% initialise RMSE array
RMSEcell= cell(length(Layer1),length(Layer2),length(Layer3),NNTraining);

% load data
fiName = append("processedData_noVik_v22", "_",num2str(eThresh(counter)),".mat");
load(fiName);

% get iterations for all permutations of layer sizes and repeat each
% NNtrainging number of times
for layer1Counter = 1:length(Layer1)
    for layer2Counter =1:length(Layer2)
     for layer3Counter = 1:length(Layer3)
        for i=1:NNTraining

rng('shuffle');

% assign layersize for the current iteration
layer1 = Layer1(layer1Counter);
layer2 = Layer2(layer2Counter);
layer3 = Layer3(layer3Counter);

% get layersize array 
% (Only fill layer 2 if it is non zero)
%( Only fill layer 3 if all preceding layers and itself are non zero)
 if layer3 == 0
     if layer2 == 0
         Layersize = [layer1];
     else
         Layersize= [layer1 layer2];
     end
 else
     if layer2 == 0
         Layersize=[layer1];
     else
     Layersize = [layer1 layer2 layer3];
     end
 end
 % don't train if layer 3 is non zero and layer 2 is zero
 if (layer2 == 0 && layer3 ~=0) 
 else
    
    % create untrained net using the layersize array
    net = fitnet(Layersize, "trainbr");

    % set max fail parameter
    maxFail = 100;

    % set function that divides input data
    net.divideFcn='divideblock';

    % set dataset sizes
    net.divideParam.trainRatio = 0.89999;
    net.divideParam.valRatio = 1-0.89999;
    net.divideParam.testRatio = 0;

    % assign maxfail parameter
    net.trainParam.max_fail= maxFail;

    % Create input/ouput data arrays
    Input=[Xtrain;Xval]';
    Output=[Ytrain;Yval]';

    % set num of epochs
    net.trainParam.epochs = 100;

    % hide training window
    net.trainParam.showWindow=0;

    % assign activation functions depending on which layers are used
    % transfer function list:
    %     compet - Competitive transfer function.
    %     elliotsig - Elliot sigmoid transfer function.
    %     hardlim - Positive hard limit transfer function.
    %     hardlims - Symmetric hard limit transfer function.
    %     logsig - Logarithmic sigmoid transfer function.
    %     netinv - Inverse transfer function.
    %     poslin - Positive linear transfer function.
    %     purelin - Linear transfer function.
    %     radbas - Radial basis transfer function.
    %     radbasn - Radial basis normalized transfer function.
    %     satlin - Positive saturating linear transfer function.
    %     satlins - Symmetric saturating linear transfer function.
    %     softmax - Soft max transfer function.
    %     tansig - Symmetric sigmoid transfer function.
    %     tribas - Triangular basis transfer function.

    net.layers{1}.transferFcn = "logsig";
    if length(Layersize)==2
        net.layers{2}.transferFcn = "logsig";
    elseif length(Layersize)==3
        net.layers{2}.transferFcn = "logsig";
        net.layers{3}.transferFcn = "logsig";
    end
    
    % train net
    [trainedNet,tr] = train(net,Input,Output);
    
    % get ypred + error
    Ypred = trainedNet(Xtest');
    error = Ypred'-Ytest;
    
    % get current nRMSE of error
    RMSE = sqrt(mean(error.^2));
    
    % write RMSE to array
    RMSEcell{layer1Counter,layer2Counter,layer3Counter,i}=RMSE;
    
    % saving each iteration (every NN that is trained on current topology
    % assemble save name
    fileNameSave = append(currOutfiName,num2str(layer1),'_',num2str(layer2),'_',num2str(layer3),'_',num2str(i));
    saveParfor(fpath,fileNameSave,tr,trainedNet,error,RMSE,Xtrain,Xval,Xtest,Yval,Ytrain,Ytest);
 end
end
end
end
end
end

function saveParfor(path,fileNameSave,tr,trainedNet,error,RMSE,Xtrain,Xval,Xtest,Yval,Ytrain,Ytest)

save(fullfile(path,fileNameSave), "tr","trainedNet","error","RMSE",...
    "Xtrain","Xval","Xtest","Yval","Ytrain","Ytest");
end