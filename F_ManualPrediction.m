% % Determine where your m-file's folder is.
% %find the filepath
ItsMe = mfilename;
mainDir = fileparts(which(ItsMe));

% Add that folder plus all subfolders to the path.
addpath(genpath(mainDir));

% 
% % load workspace if necessary...
% % header string array and cell array of nets required
% % check the y for a certain composition, using the net

defaultants = cellstr(num2str(zeros(length(ElementsHeaderLeft),1)));
Xusr = str2double(inputdlg(cellstr(ElementsHeaderLeft),...
    'Enter Composition', 0.75, defaultants));
Xusr = hunToOne(Xusr);

%%

% No need to normalize for latest version!
% Xusr = ((Xusr'-minComp)./(maxComp-minComp))'; %normalize according to maximum and minimum composition
% resMat = zeros(length(Networks),3); %preallocate
% [numRow, numCol] = size(resMat);
for ii = 1:numRow
    resMat(ii,:) = Networks{ii}(Xusr)';
end

AvrgResult = mean(resMat);

% for jj = 1:numCol
%     AvrgResult(jj) = round(denormalize(maxTemp(jj), minTemp(jj), AvrgResult(jj)), 0);
% end


msgtxt = sprintf('Operation Completed, result: \n Tg = %.0f K \n Tx = %.0f K\n Tl  = %.0f K', AvrgResult);
f = msgbox(msgtxt);


