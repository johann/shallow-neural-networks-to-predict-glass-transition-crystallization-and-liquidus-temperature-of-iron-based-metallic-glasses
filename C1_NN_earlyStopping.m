fileName = 'NN_20x20_Epoch_MaxFail_';
folderName ="/save/NN";

RMSE_Epoch_Maxfail = cell(8,2,25);

% Array of # of Epochs to test
Epochs = [10 20 30 50 75 100 500 1000];

% parameter array for early stopping
MaxFail = [0 1 2];

% load data
load("processedData_v21.mat");

for EpochCounter = 1:8
    epochs  = Epochs(EpochCounter);

    for failCounter = 2:2

        for i=1:size(RMSE_Epoch_Maxfail,3)

rng('shuffle');

% number of hidden layers
hiddenLayerSize = [10 10];

% create untrained neural network
net = fitnet(hiddenLayerSize, "trainbr");

        if MaxFail(failCounter) == 0

            % create untrained neural network
            net = fitnet(hiddenLayerSize, "trainbr");
            maxFail = 50;

            % set function to divide datasets
            net.divideFcn='divideblock';


            % set ratio of datasets
            net.divideParam.trainRatio = 0.89999;
            net.divideParam.valRatio = 1-0.89999;
            net.divideParam.testRatio = 0;
            % set max fail parameter for early stopping
            net.trainParam.max_fail= maxFail;
    
            % submit input and output values to net for training
            Input=[Xtrain;Xval]';
            Output=[Ytrain;Yval]';
        elseif MaxFail(failCounter) == 1

            % create untrained neural network
            net = fitnet(hiddenLayerSize, "trainbr");

            % submit input and output values to net for training
            Input=[Xtrain;Xval]';
            Output=[Ytrain;Yval]';
        elseif MaxFail(failCounter) == 2

            % create untrained neural network
            net = fitnet(hiddenLayerSize, "trainlm");

            % early stopping parameter
            maxFail = 50;

            % function for dividing data into datasets
            net.divideFcn='divideblock';

            % set ratio of datasets
            net.divideParam.trainRatio = 0.89999;
            net.divideParam.valRatio = 1-0.89999;
            net.divideParam.testRatio = 0;
            net.trainParam.max_fail= maxFail;

            % submit input and output values to net for training
            Input=[Xtrain;Xval]';
            Output=[Ytrain;Yval]';
        end

% set num epochs
net.trainParam.epochs = epochs;

% don't show console windows
net.trainParam.showWindow=0;

% set activation function
net.layers{1}.transferFcn = "logsig";
net.layers{2}.transferFcn = "logsig";

% train untrained net
[trainedNet,tr] = train(net,Input,Output);

% get y pred
Ypred = trainedNet([Xtest]');
% find vector of errors
error = Ypred'-[Ytest];

% calculate RMSE of error
RMSE = sqrt(mean(error.^2));

% un-normalise the RMSE using the temperature range
RMSE_absolut = RMSE.*(maxTemp-minTemp);

% add RMSE to table
RMSE_Epoch_Maxfail{EpochCounter,failCounter,i}=RMSE_absolut;

% assemble save filename and path
fileNameSave = append(fileName,num2str(epochs),'_',num2str(failCounter),'_',num2str(i));
[path,~,~]=fileparts(which(mfilename));
path = append(path,folderName);

% create path if not exists
if ~exist(path, 'dir')
       mkdir(path)
 end

% save file
save(fullfile(path,fileNameSave), "tr","trainedNet","error","RMSE","maxTemp","minTemp",...
    "Xtrain","Xval","Xtest","Yval","Ytrain","Ytest");


        end
    end
end

% save(fullfile(path,fileName), "Epochs","MaxFail","RMSE_Epoch_Maxfail");