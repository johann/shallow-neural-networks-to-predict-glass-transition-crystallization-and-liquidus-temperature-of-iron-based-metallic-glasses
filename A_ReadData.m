clc
clear all

% % Determine where your m-file's folder is.
% %find the filepath
mainDir = fileparts(which(mfilename));

%add paths
addpath(genpath(pwd));

%filename
[fiName, fpath] = uigetfile('*.*'); % Script is based on an data in .xls file.

% where to save read in and processed data
foName ="\save";

%% read alloys sheet from xlsx

% the alloys sheet contains the compositon formulae for all alloys
% (input values)
% and the relevant temperatures and critical thickness
% (output values)
Alloys = readtable(fiName,'Sheet','Alloys', 'VariableNamingRule','preserve');
Alloys(isnan(Alloys.ID),:) = []; % delete rows which do not have an id
Alloys = delEmpties(Alloys); % delete fully empty columns and rows

% read in enthalpies of mixing & radii difference sheets & radii
% classification
Enthalpy = readtable(fiName, 'Sheet', 'Enthalpies of Mixing', 'VariableNamingRule','preserve');
Enthalpy = delEmpties(Enthalpy);

AtomicRadii = readtable(fiName, 'Sheet', 'Atomic radii', 'VariableNamingRule','preserve');
AtomicRadii = delEmpties(AtomicRadii);

AtomicRadiiClassification = readtable(fiName, 'Sheet', 'Radius Classification', 'VariableNamingRule','preserve');
AtomicRadiiClassification = delEmpties(AtomicRadiiClassification);

%% composition is a long cell array
Composition = Alloys.(2); %Alloy compositions in 2nd column

% determine the number of samples
len = numel(Alloys.ID);

% determine the compositions as mass fractions and save in array with
% height = number of samples and width = chemical elements 
compCelMat =  cell(len,1);
for i=1:len
    [currentComp, ~] = calcComp(string(Composition{i}));
    compCelMat{i} = currentComp;
end

%% create element header array

% initialise
ElementsHeader = string([]);

for kk=1:len
    % add all elements not yet contained in the header to the header
    currentElements = compCelMat{kk}(:,1);
    % Len = size(currentElements);    
    lia = ismember(currentElements, ElementsHeader);
    ElementsHeader = [ElementsHeader; currentElements(~lia)];
end
%% initialise input matrix
Inputs = zeros(len, length(ElementsHeader));
    
% iterate over all samples
 for i=1:len   

    % loop over elements in the composition
    for j = 1:length(compCelMat{i})
        % get name of element
        elementName = compCelMat{i}(j, 1);
        elementName = elementName{1};

        % find position of element in element header
        elementNumber = find(strcmp(ElementsHeader, elementName));

        % get mass fraction of element in the sample and convert to double
        currentCompValue = compCelMat{i}(j, 2);
        currentCompValue = str2double(currentCompValue{1});

        % fill the array with the mass fraction
        Inputs(i, elementNumber) = currentCompValue;
    end
 end

 % some mass fractions are given as percentage. convert all to decimal
 for i = 1:len
     Inputs(i,:) = hunToOne(Inputs(i,:));
 end

%%  read output values and ID from sheet array
Tg = str2double(Alloys.(5)); % in Kelvin
Tx = str2double(Alloys.(6)); % in Kelvin
Tl = str2double(Alloys.(7)); % in Kelvin
hr = str2double(Alloys.(9)); % in Kelvin/min

% form array header string array in the same order as input/output array
arrayheader = ["ID", ElementsHeader{:,1}, "Tg", "Tx", "Tl", "HeatingRate"];

% use header names and array to create a table
Data_tbl = array2table([Alloys.ID, Inputs ,Tg ,Tx ,Tl, hr],'VariableNames', arrayheader);

%% save
[~, fiName, ~] = fileparts(fiName);
fiName = append(fiName,'.mat');
fpath = append(fpath,foName);


% save variables to matlab file
save(fullfile(fpath,fiName), "Data_tbl", "AtomicRadii", "Enthalpy", "ElementsHeader", "AtomicRadiiClassification");
